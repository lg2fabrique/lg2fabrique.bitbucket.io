# Composer repository

The `packages.json` file in this repository acts like a self-hosted packagist.org. You need to manually edit this file
if you want to add a package.

https://getcomposer.org/doc/05-repositories.md#repositories

### Packages available

**lg2/craft**

```bash
composer create-project lg2/craft --remove-vcs --repository-url=https://lg2fabrique.bitbucket.io your-project-name
```
